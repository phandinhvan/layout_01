<?php

namespace Layout_01\Plugin\Render;

class Render
{

	function renderLayout()
	{

		// read file style and layout
		// $pathStyle = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/styles/style.txt";
		// $pathLayout = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/layout/layout.html";
		$pathStyle =  __DIR__ . "/" . "../../styles/style.txt";
		$pathLayout =  __DIR__ . "/" . "../../layout/layout.html";
		$style = htmlentities(file_get_contents($pathStyle));
		$layout = htmlentities(file_get_contents($pathLayout));

		// $servername = "localhost";
		// $username = "root";
		// $password = "";
		// $dbname = "db_layout_01";

		$servername = "mysql";
		$username = "root";
		$password = "pimmm2021";
		$dbname = "db_layout_01";

		// Create connection
		$conn = new \mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}

		$str = "$style";
		$str .= "<div class='contain'>";

		$sql = "SELECT * FROM users";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			// output data of each row
			while ($row = $result->fetch_assoc()) {
				$fields = ["_id", "_firstname", "_lastname", "_email"];
				$rows   = [$row["id"], $row["firstname"], $row["lastname"], $row["email"]];
				$newStr = str_replace($fields, $rows, $layout);
				$str .= $newStr;
			}
		} else {
			echo "0 results";
		}

		$str .= "</div>";

		$conn->close();

		$htmlStr = html_entity_decode($str);

		return $htmlStr;
	}
}
