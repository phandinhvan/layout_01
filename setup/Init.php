<?php

namespace Layout_01\Setup;

class Init
{

	function createDB()
	{
		// $servername = "localhost";
		// $username = "root";
		// $password = "";

		$servername = "mysql";
		$username = "root";
		$password = "pimmm2021";

		// Create connection
		$conn = new \mysqli($servername, $username, $password);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}

		// Create database
		$sql = "CREATE DATABASE IF NOT EXISTS db_layout_01";
		if ($conn->query($sql) === TRUE) {
			echo "Database created successfully <br />";
			$this->migrationDB($servername, $username, $password, "db_layout_01");
		} else {
			echo "Error creating database: " . $conn->error;
		}

		$conn->close();
	}

	function migrationDB($servername, $username, $password, $database)
	{
		$conn = new \mysqli($servername, $username, $password, $database);
		// sql code to create table
		$table = "users";
		$result = $conn->query("SHOW TABLES LIKE '" . $table . "'");

		if ($result->num_rows == 1) {
			echo "Table exists";
		} else {
			$sql = "CREATE TABLE users(
        id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
        firstname VARCHAR(30) NOT NULL,
        lastname VARCHAR(30) NOT NULL,
        email VARCHAR(50)
      )";

			if ($conn->query($sql) === TRUE) {
				echo "Table users created successfully <br />";
				$this->insertUser($conn);
			} else {
				echo "Error creating table: " . $conn->error;
			}
		}
	}

	function insertUser($conn)
	{
		$sql = "INSERT INTO users (firstname, lastname, email)
    VALUES ('John1', 'Doe1', 'john1@example.com');";

		for ($i = 2; $i < 101; $i++) {
			$firstName = "John" . $i;
			$lastName = "Doe" . $i;
			$email = $firstName . "@example.com";
			$sql .= " INSERT INTO users (firstname, lastname, email)
      VALUES ('$firstName', '$lastName', '$email');";
		}

		if ($conn->multi_query($sql) === TRUE) {
			echo "New records created successfully";
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}
	}
}
